# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  $("#submit").click ->
    $.getJSON "/CarSearches" + "/" + $("#cname").val(), {}, (json, response) ->
      if json is false
        alert("Oops, no customer found yet")
      else
        window.location.replace("/customers/"+json)
      return
    return
  return


$(document).ready(ready)
$(document).on('page:load', ready)



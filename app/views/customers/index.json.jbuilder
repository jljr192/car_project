json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :car_id, :finance, :finance_id, :dob, :address
  json.url customer_url(customer, format: :json)
end

json.array!(@owners) do |owner|
  json.extract! owner, :id, :total_gross_revenue, :net_profit, :sales_tax_total
  json.url owner_url(owner, format: :json)
end

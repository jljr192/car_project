json.array!(@managers) do |manager|
  json.extract! manager, :id, :name, :department, :hire_date
  json.url manager_url(manager, format: :json)
end

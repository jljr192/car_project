json.array!(@sales) do |sale|
  json.extract! sale, :id, :customer_id, :car_id, :worker_id, :sold
  json.url sale_url(sale, format: :json)
end

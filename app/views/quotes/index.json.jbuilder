json.array!(@quotes) do |quote|
  json.extract! quote, :id, :worker_id, :customer_id, :car_id, :price, :sale_tax, :total_price, :quote_status
  json.url quote_url(quote, format: :json)
end

json.array!(@workers) do |worker|
  json.extract! worker, :id, :name, :hire_date, :department, :area
  json.url worker_url(worker, format: :json)
end

json.extract! @car, :id, :brand, :model, :vin, :msrp, :price, :acquired, :sold, :created_at, :updated_at

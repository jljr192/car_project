json.array!(@cars) do |car|
  json.extract! car, :id, :brand, :model, :vin, :msrp, :price, :acquired, :sold
  json.url car_url(car, format: :json)
end

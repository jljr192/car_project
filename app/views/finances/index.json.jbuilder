json.array!(@finances) do |finance|
  json.extract! finance, :id, :sai, :ppy, :year
  json.url finance_url(finance, format: :json)
end

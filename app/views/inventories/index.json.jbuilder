json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :manager_id, :car_id
  json.url inventory_url(inventory, format: :json)
end

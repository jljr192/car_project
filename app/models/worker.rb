class Worker < ActiveRecord::Base
  belongs_to :manager
  has_many :quotes
end

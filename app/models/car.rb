class Car < ActiveRecord::Base

  has_many :inventories
  has_many :customer
  has_many :managers, through: :inventory
  has_one :sale
  has_many :quotes
end
def car
  @cars = Car.all
end


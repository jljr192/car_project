class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :customer_id
      t.integer :car_id
      t.integer :worker_id
      t.string :sold

      t.timestamps null: false
    end
  end
end

class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :car_id
      t.string :finance
      t.integer :finance_id
      t.date :dob
      t.string :address

      t.timestamps null: false
    end
  end
end

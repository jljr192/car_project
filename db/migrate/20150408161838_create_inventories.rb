class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :manager_id
      t.integer :car_id

      t.timestamps null: false
    end
  end
end

class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :worker_id
      t.integer :customer_id
      t.integer :car_id
      t.decimal :price
      t.decimal :sale_tax
      t.decimal :total_price
      t.boolean :quote_status

      t.timestamps null: false
    end
  end
end

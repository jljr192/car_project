class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :name
      t.date :hire_date
      t.string :department
      t.string :area

      t.timestamps null: false
    end
  end
end

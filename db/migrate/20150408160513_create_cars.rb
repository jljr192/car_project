class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :brand
      t.string :model
      t.integer :vin
      t.decimal :msrp
      t.decimal :price
      t.date :acquired
      t.date :sold

      t.timestamps null: false
    end
  end
end

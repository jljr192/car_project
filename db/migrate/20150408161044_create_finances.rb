class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.decimal :sai
      t.integer :ppy
      t.integer :year

      t.timestamps null: false
    end
  end
end

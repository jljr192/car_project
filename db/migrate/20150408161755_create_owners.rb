class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.integer :total_gross_revenue
      t.integer :net_profit
      t.integer :sales_tax_total

      t.timestamps null: false
    end
  end
end

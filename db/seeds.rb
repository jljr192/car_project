# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Customer.create(name: 'Double Diamond', car_id: '03', finance: 'No', finance_id: '$12,345.22', dob: '1993-02-12', address: '123 Equality Ln')
# Customer.create(name: 'Pinkie Pie', car_id: '02', finance: 'Yes', finance_id: '$1,345.22', dob: '1992-04-14', address: '54 SugarCube Corner')
# Customer.create(name: 'Twilight Sparkle', car_id: '01', finance: 'No', finance_id: '$12,345.22', dob: '1992-02-01', address: '1 Golden Oak Rd')

Car.create(brand: 'Volvo', model: 'S60',vin:'12376534', msrp: '23995.54', price: '27995.43', acquired: '2015-03-01', sold: '2015-04-21')
Car.create(brand: 'Toyota', model: 'Tacoma',vin:'23476534', msrp: '13995.45', price: '17995.99', acquired: '2015-03-11', sold: '2015-04-15')
Car.create(brand: 'Chevrolet', model: 'Malibu',vin:'23376533', msrp: '12995.54', price: '13995.75', acquired: '2015-04-01', sold: '2015-04-21')
Car.create(brand: 'Ford', model: 'Fiesta',vin:'2376534', msrp: '9995.74', price: '12995.43', acquired: '2015-03-21', sold: '2015-04-01')
Car.create(brand: 'Fiat', model: '500',vin:'76376534', msrp: '8995.54', price: '10995.43', acquired: '2015-04-03', sold: '2015-04-11')
Car.create(brand: 'Ford', model: 'Fiesta',vin:'42366431', msrp: '13995.45', price: '13995.99', acquired: '2015-04-01', sold: '2015-04-01')
Car.create(brand: 'Toyota', model: 'Camry',vin:'87237653', msrp: '13995.54', price: '15995.33', acquired: '2015-04-02', sold: '2015-04-11')
Car.create(brand: 'Honda', model: 'Civic',vin:'92376556', msrp: '14945.99', price: '17595.53', acquired: '2015-02-10', sold: '2015-04-21')
